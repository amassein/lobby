# FLASK APP
import functions as fct_lob
from flask import Flask
from markupsafe import escape
import networkx as nx
import json

def built_graph(lobby):
    colors, weight, net, cli, cli_de, affi = fct_lob.lobby_actions(lobby, 1)
    # write json formatted data
    d = nx.json_graph.node_link_data(net)  # node-link format to serialize
    # write json
    json.dump(d, open("force/force.json", "w"))
    print("Wrote node-link JSON data to force/force.json")
    return(cli, cli_de, affi)

app = Flask(__name__, static_folder="force")

@app.route("/")
def hello_world():
    liste = [f"<li><a href='graph_{lob}'>{lob}</a></li>" for lob in fct_lob.dico_action.keys() if len(fct_lob.dico_action[lob]['clients']) > 1]
    lenght = len(liste)
    liste = ''.join(liste)
    return f"<h1>Hello, World!</h1><p><h4>Nombre de lobby : {lenght}</h4></p><p><ul>{liste}<ul></p>"

@app.route("/<string:lobby>")
def build_graph(lobby):
    return(f"url = <a href='/graph_{lobby}'>graph_{lobby}</a>")

@app.route("/graph_<string:lobby>")
def static_proxy(lobby):
    cli, cli_de, affi = built_graph(lobby)
    return app.send_static_file("force.html")
