import requests
import os
import time
import json
import pandas as pd
import hvplot.pandas
import holoviews as hv
import hvplot.networkx as hvnx
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx

temp = requests.get("https://www.hatvp.fr/agora/opendata/agora_repertoire_opendata.json").json()
print("Internet load")

def load_dico():

    dico_lobby = {}
    for i in range(len(temp['publications'])):

        diri_info = []
        for pers in temp['publications'][i]['dirigeants']:
            indiv = ' '.join([pers['nom'], pers['prenom']])
            if indiv in diri_info:
                pass
            else:
                diri_info.append(indiv)

        collab_info = []
        for pers in temp['publications'][i]['collaborateurs']:
            indiv = ' '.join([pers['nom'], pers['prenom']])
            if indiv in collab_info:
                pass
            else:
                collab_info.append(indiv)

        dico_lobby[temp['publications'][i]['denomination']] = {
        #, temp['publications'][i].keys())
        'dirigeants' : diri_info,#[diri for diri in temp['publications'][i]['dirigeants']],
        'collaborateurs' : collab_info,#[collab for collab in temp['publications'][i]['collaborateurs']],
        'clients' : [cli['denomination'] for cli in temp['publications'][i]['clients']],
        #'declarationTiers' : temp['publications'][i]['declarationTiers'],
        #'declarationOrgaAppartenance' : temp['publications'][i]['declarationOrgaAppartenance'],
        'affiliations' : [affi['denomination'] for affi in temp['publications'][i]['affiliations']],
        "activité" : [acti["code"] for acti in temp['publications'][i]['activites']['listSecteursActivites']],
        "id" : i
        }
        try :
            dico_lobby[temp['publications'][i]['denomination']]['site_web'] = temp['publications'][i]['lienSiteWeb']
        except:
            pass

    return(dico_lobby)

def load_dico_action():
    tab = []

    data = temp['publications']
    dico_action = {}

    for i in range(0, len(data)):
        #if i > 10:
        #    break
        #print(f"\r{i}/{len(data)} : {data[i]['denomination']}", end="")
        dico_action[data[i]['denomination']] = {"nb_action" : int(), "clients" : [], "actions" : {}}
        count = 0
        for j in range(len(data[i]['exercices'])):
            data2 = data[i]['exercices'][j]['publicationCourante']
            try:

                #print(data2['chiffreAffaire'], ", date de publication :", data2['publicationDate'])

                for z, info in enumerate(data2['activites']):
                    objet = info['publicationCourante']['objet']
                    client = info['publicationCourante']['actionsRepresentationInteret'][0]['tiers'][0]
                    #print(f"  Action {z+1} : {objet}")
                    #print("   Client :", client)
                    if client not in dico_action[data[i]['denomination']]['clients']:
                        dico_action[data[i]['denomination']]['clients'].append(client)
                        dico_action[data[i]['denomination']]['actions'][client] = []
                    dico_action[data[i]['denomination']]['actions'][client].append(objet)
                    count += 1
            except:
                try:
                    for z, info in enumerate(data2['activites']):
                        objet = info['publicationCourante']['objet']
                        client = info['publicationCourante']['actionsRepresentationInteret'][0]['tiers'][0]
                        #print(f"Sans CA : Action {z+1} : {objet}")
                        #print("Sans CA : Client :", client)
                        if client not in dico_action[data[i]['denomination']]['clients']:
                            dico_action[data[i]['denomination']]['clients'].append(client)
                            dico_action[data[i]['denomination']]['actions'][client] = []
                        dico_action[data[i]['denomination']]['actions'][client].append(objet)
                        count += 1
                except:
                    pass

        if count > 0:
            dico_action[data[i]['denomination']]["nb_action"] = count
        else:
            del dico_action[data[i]['denomination']]

    return(dico_action)


def get_lobby_info(name_lobby):
    print(name_lobby)
    if name_lobby not in dico_lobby:
        return ("NOT IN")
    for item in dico_lobby[name_lobby]:
        print(item, ":", dico_lobby[name_lobby][item])


def get_lobby_affi(name_lobby):
    stock = []
    for item in dico_lobby:
        if name_lobby in dico_lobby[item]['affiliations']:
            # print(item)
            stock.append(item)
    return (stock)


def get_lobby_cli(name_lobby):
    stock = []
    for item in dico_lobby:
        if name_lobby in dico_lobby[item]['clients']:
            # print(item)
            stock.append(item)
    return (stock)


def get_pers_info(name_pers):
    if name_pers not in dico_pers.keys():
        return ("Not In")
    else:
        for item in dico_pers[name_pers]:
            print(item, ":")
            for elem in dico_pers[name_pers][item]:
                print("    ", elem)


def get_net_lob_cli(net, lobby):
    lobby1 = dico_action[lobby]
    for cli in lobby1['clients']:
        if cli in net:
            if net.has_edge(lobby, cli):
                net[lobby][cli]['weight'] += len(lobby1['actions'][cli])
            else:
                net.add_edge(lobby, cli, weight=len(lobby1['actions'][cli]), group='client', color=2, lob=lobby)
        elif cli not in net:
            net.add_node(cli)
            net.add_edge(lobby, cli, weight=len(lobby1['actions'][cli]), group='client', color=2, lob=lobby)
    return (net)


def get_net_lob_affi(net, lobby):
    for res in get_lobby_affi(lobby):
        if res in net:
            if net.has_edge(lobby, res):
                net[lobby][res]['group'] = net[lobby][res]['group'] + " Lob_affilié"
                net[lobby][res]['color'] = 4
            else:
                net.add_edge(lobby, res, weight=1, group='Lob_affilié', color=3, lob=lobby)
        elif res not in net:
            net.add_node(res)
            net.add_edge(lobby, res, weight=1, group='Lob_affilié', color=3, lob=lobby)

    return (net)


def get_net_lob_cli_to(net, lobby):
    for res in get_lobby_cli(lobby):
        if res in net:
            if net.has_edge(lobby, res):
                net[lobby][res]['weight'] += 1
            else:
                net.add_edge(lobby, res, weight=1, group='client_de', color=1, lob=lobby)
        elif res not in net:
            net.add_node(res)
            net.add_edge(lobby, res, weight=1, group='client_de', color=1, lob=lobby)

    return (net)


def show_clients(lobby):
    cli = input("Quel client voulez-vous consulter ? : ")
    if cli == 'exit':
        return ('Out')
    elif cli not in dico_action[lobby]['clients']:
        print(dico_action[lobby]['clients'])
        print("Le client indiqué n'existe pas. Quel client voulez-vous consulter ? : ")
        show_clients(lobby)
    else:
        for i, act in enumerate(dico_action[lobby]['actions'][cli]):
            print(f"Action {i + 1} pour {cli}: \n - {act}")
        continue_fct(lobby)


def continue_fct(lobby):
    boolean = input("Voulez-vous consulter un autre client ? (True/False) : ")
    # print(boolean)
    if boolean == 'True':
        print(dico_action[lobby]['clients'])
        show_clients(lobby)
    elif boolean == 'False':
        return ('Out')
    else:
        print("wrong entry, try again")
        continue_fct(lobby)


def graph_lobby(lobby, iteration):
    net = nx.Graph()
    for lob in dico_action[lobby]['clients']:
        if lob not in net:
            net.add_node(lob)
    net.add_node(lobby)

    net = get_net_lob_cli(net, lobby)
    net = get_net_lob_affi(net, lobby)
    net = get_net_lob_cli_to(net, lobby)

    for i in range(iteration):
        list_net = list(net.nodes)

        for res in list_net:
            if res == lobby:
                pass
            else:
                if res in dico_action.keys():
                    net = get_net_lob_cli(net, res)
                net = get_net_lob_affi(net, res)
                net = get_net_lob_cli_to(net, res)

    colors = [net[i][j]['color'] for i, j in net.edges]
    weight = [net[i][j]['weight'] for i, j in net.edges]
    plot = hvnx.draw(net,
                     node_color='lightblue',
                     with_labels=True,
                     # edge_cmap = plt.cm.BrBG,
                     edge_color=colors)
    return (colors, weight, net)


def lobby_actions(lobby, iteration):
    # lobby = input("Quel lobby voulez-vous consulter ? : ")
    # if lobby == 'exit':
    #    return(None, None, None)
    # elif lobby not in dico_action.keys():
    #    print("wrong entry, try again")
    #    colors, weight, net = lobby_actions(iteration)
    # else:
    colors, weight, net = graph_lobby(lobby, iteration)
    cli = dico_action[lobby]['clients']
    cli_de = get_lobby_cli(lobby)
    affi = get_lobby_affi(lobby)
    # show_clients(lobby)
    return (colors, weight, net, cli, cli_de, affi)

hv.notebook_extension('bokeh');

dico_action = load_dico_action()
dico_lobby = load_dico()
print("load succesfully")